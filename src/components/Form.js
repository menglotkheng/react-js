import React, { Component } from "react";
import { Table } from "./Table";
import Swal from "sweetalert2";
import "animate.css";

export class Form extends Component {
  constructor() {
    super();
    this.state = {
      user: [],

      uermail: "",
      name: "",
      age: "",
    };
  }

  onchangeMail = (e) => {
    this.setState(
      {
        uermail: e.target.value,
      },
      () => console.log(this.state.uermail)
    );
  };

  onchangeName = (e) => {
    this.setState(
      {
        name: e.target.value,
      },
      () => console.log(this.state.name)
    );
  };

  onchangeAge = (e) => {
    this.setState(
      {
        age: e.target.value,
      },
      () => console.log(this.state.age)
    );
  };

  onSubmit = () => {
    const obj = {
      id: this.state.user.length + 1,
      email: this.state.uermail,
      username: this.state.name,
      age: this.state.age,
      status: "Pending",
    };

    this.setState({
      user: [...this.state.user, obj],
      uermail: "",
      name: "",
      age: "",
    });
  };

  btStatus = (e) => {
    e.status === "Pending" ? (e.status = "Done") : (e.status = "Pending");
    this.setState({
      user: [...this.state.user],
    });
    console.log(e);
  };

  showMore = (user) => {
    Swal.fire({
      title:
        "ID: " +
        `${user.id}` +
        "<br>" +
        "Email: " +
        `${user.email === "" ? "NULL" : user.email}` +
        "<br>" +
        "Username: " +
        `${user.username === "" ? "NULL" : user.username}` +
        "<br>" +
        "Age: " +
        `${user.age === "" ? "NULL" : user.age}`,
      showClass: {
        popup: "animate__animated animate__fadeInDown",
      },
      hideClass: {
        popup: "animate__animated animate__fadeOutUp",
      },
    });
  };

  render() {
    return (
      <div>
        <div class="w-6/12  m-auto h-auto mt-10">
          <form>
            <div class="mb-6">
              <label
                for="email"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Your email
              </label>
              <input
                onChange={this.onchangeMail}
                type="email"
                id="email"
                class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                placeholder="menglot@gmail.com"
                required
              />
            </div>
            <div class="mb-6">
              <label
                for="password"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Your Username
              </label>
              <input
                onChange={this.onchangeName}
                type="text"
                id="username"
                class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                required
              />
            </div>
            <div class="mb-6">
              <label
                for="repeat-password"
                class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Age
              </label>
              <input
                onChange={this.onchangeAge}
                type="text"
                id="age"
                class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                required
              />
            </div>

            <button
              onClick={this.onSubmit}
              type="submit"
              class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            >
              Register new account
            </button>
          </form>
        </div>
        <Table
          tableInfo={this.state.user}
          btn={this.showMore}
          data3={this.btStatus}
        ></Table>
      </div>
    );
  }
}
