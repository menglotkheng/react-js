import { Component } from "react";

export class Table extends Component {
  render() {
    return (
      <div class="w-6/12  m-auto h-auto mt-10">
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-5">
          <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase dark:text-gray-400">
              <tr>
                <th scope="col" class="px-6 py-3 bg-gray-50 dark:bg-gray-800">
                  ID
                </th>
                <th scope="col" class="px-6 py-3 bg-gray-50">
                  Email
                </th>
                <th scope="col" class="px-6 py-3 bg-gray-50 dark:bg-gray-800">
                  Username
                </th>
                <th scope="col" class="px-6 py-3 bg-gray-50 dark:bg-gray-800">
                  Age
                </th>
                <th scope="col" class="px-6 py-3 bg-gray-50">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.tableInfo.map((item) => (
                <tr
                  class={`border-b border-gray-200 dark:border-gray-700 ${
                    item.id % 2 === 0 ? `bg-red-400` : `bg-white-500`
                  }`}
                >
                  <th
                    scope="row"
                    class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap  dark:text-white dark:bg-gray-800 "
                  >
                    {item.id === "" ? "NULL" : item.id}
                  </th>
                  <td class="px-6 py-4">
                    {item.email === "" ? "NULL" : item.email}
                  </td>
                  <td class="px-6 py-4  dark:bg-gray-800">
                    {item.username === "" ? "NULL" : item.username}
                  </td>
                  <td class="px-6 py-4  dark:bg-gray-800">
                    {item.age === "" ? "NULL" : item.age}
                  </td>
                  <td class="px-6 py-4 ">
                    <button
                      type="button"
                      class={
                        item.status == "Pending"
                          ? "text-white bg-red-600  focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-80"
                          : "text-white bg-green-600  focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-80"
                      }
                      onClick={() => {
                        this.props.data3(item);
                      }}
                    >
                      {item.status}
                    </button>
                    <button
                      onClick={() => {
                        this.props.btn(item);
                      }}
                      type="button"
                      class="text-white bg-blue-700 hover:bg-blue-500 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:hover:bg-gray-700 dark:focus:ring-gray-700 dark:border-gray-700"
                    >
                      Show More
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
